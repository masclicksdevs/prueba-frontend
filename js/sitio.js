
var c = {
    test : true,
    log : function(string) {
        if( this.test ) console.log(string);
    }
},

siguientePaso = function(e){
    e.preventDefault();
    var padre, siguiente;

    try {

        padre = $(this).parents('.section');
        siguiente = padre.next('.section');
        
        if( siguiente.length != 1 )  {
            if(typeof(Storage) !== "undefined") {
                var numero = 1;
                if ( localStorage.getItem('test') ) {
                    numero = Number(localStorage.getItem('test')) + numero;
                }
                localStorage.setItem('test', numero);
            }
            throw '[Error] No hay más elementos';
        }

        padre.removeClass('activo');
        siguiente.addClass('activo');


    } catch(error) {
        c.log(error);
    }

},

final = function(e){
    e.preventDefault();
    var cantidad = '';
    c.log('Cantidad indicada: ' + cantidad);
    alert(cantidad);
},

iniciar = function(){
    c.log('Inicia la prueba frontend');
    $('button').on('click', siguientePaso);
    $('button.ultimo').on('click', final);
};


$(document).on('ready', inicia);